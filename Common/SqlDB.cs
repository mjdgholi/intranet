﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Intranet.Common
{
    public class SqlDB : IntranetDB
    {
        protected SqlConnection Connection;

        public SqlDB(string connectionstring)
        {
            base.ConnectionString = connectionstring;
            this.Connection = new SqlConnection(base.ConnectionString);
        }

        protected SqlCommand BuildQueryCommand(string storedprocedureName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedprocedureName, this.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            foreach (SqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        protected SqlCommand BuildQueryCommand(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            SqlCommand command = new SqlCommand(storedprocedureName, this.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            foreach (SqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            command.Parameters.Add(paramOutput);
            return command;
        }

        public override DataRow Execute(string strSQL)
        {
            DataSet set = this.Execute(strSQL, "temp");
            DataRow row = set.Tables["temp"].NewRow();
            if (set.Tables["temp"].Rows.Count > 0)
            {
                row = set.Tables["temp"].Rows[0];
            }
            return row;
        }

        public override DataSet Execute(string strSQL, string tablename)
        {
            SqlDataAdapter adapter = new SqlDataAdapter(strSQL, this.Connection);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, tablename);
            }
            finally
            {
                if (adapter.SelectCommand.Connection.State == ConnectionState.Open)
                {
                    adapter.SelectCommand.Connection.Close();
                }
            }
            return dataSet;
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype)
        {
            return new SqlParameter(parametername, this.GetSQLDBType(parametertype));
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype, object parameterevalue)
        {
            return new SqlParameter(parametername, this.GetSQLDBType(parametertype)) { Value = parameterevalue };
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype, int size, object parameterevalue)
        {
            return new SqlParameter(parametername, this.GetSQLDBType(parametertype), size) { Value = parameterevalue };
        }

        public SqlDbType GetSQLDBType(DataTypes ptype)
        {
            if (ptype == DataTypes.bit)
            {
                return SqlDbType.Bit;
            }
            if (ptype == DataTypes.bigint)
            {
                return SqlDbType.BigInt;
            }
            if (ptype == DataTypes.character)
            {
                return SqlDbType.Char;
            }
            if (ptype == DataTypes.datetime)
            {
                return SqlDbType.DateTime;
            }
            if (ptype == DataTypes.image)
            {
                return SqlDbType.Image;
            }
            if (ptype == DataTypes.integer)
            {
                return SqlDbType.Int;
            }
            if (ptype == DataTypes.nvarchar)
            {
                return SqlDbType.NVarChar;
            }
            if (ptype == DataTypes.smallint)
            {
                return SqlDbType.SmallInt;
            }
            if (ptype == DataTypes.varchar)
            {
                return SqlDbType.VarChar;
            }
            if (ptype == DataTypes.ntext)
            {
                return SqlDbType.NText;
            }
            return SqlDbType.Variant;
        }

        public override bool RunProcedure(string storedprocedureName, IDataParameter[] parameters)
        {
            SqlCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            try
            {
                if (command.Connection.State == ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return true;
        }

        public override int RunProcedure(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            SqlCommand command = this.BuildQueryCommand(storedprocedureName, parameters, paramOutput);
            try
            {
                if (command.Connection.State == ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return (int)paramOutput.Value;
        }

        public override DataRow RunProcedureDR(string storedprocedureName, IDataParameter[] parameters)
        {
            DataSet set = this.RunProcedureDS(storedprocedureName, parameters);
            DataRow row = set.Tables[0].NewRow();
            if (set.Tables[0].Rows.Count > 0)
            {
                row = set.Tables[0].Rows[0];
            }
            return row;
        }

        public override DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters)
        {
            SqlDataAdapter adapter = new SqlDataAdapter
            {
                SelectCommand = this.BuildQueryCommand(storedprocedureName, parameters)
            };
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "TableName");
            }
            finally
            {
                if (adapter.SelectCommand.Connection.State == ConnectionState.Open)
                {
                    adapter.SelectCommand.Connection.Close();
                }
            }
            return dataSet;
        }

        public override DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters, string tablename)
        {
            SqlDataAdapter adapter = new SqlDataAdapter
            {
                SelectCommand = this.BuildQueryCommand(storedprocedureName, parameters)
            };
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, tablename);
            }
            finally
            {
                if (adapter.SelectCommand.Connection.State == ConnectionState.Open)
                {
                    adapter.SelectCommand.Connection.Close();
                }
            }
            return dataSet;
        }

        public override DataTable RunProcedureDT(string storedprocedureName, IDataParameter[] parameters)
        {
            SqlCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            DataTable schemaTable = new DataTable();
            try
            {
                this.Connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                schemaTable = reader.GetSchemaTable();
                while (reader.Read())
                {
                    DataRow row = schemaTable.NewRow();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        row[i] = reader[i];
                    }
                    schemaTable.Rows.Add(row);
                }
                reader.Close();
            }
            finally
            {
                if (this.Connection.State == ConnectionState.Open)
                {
                    this.Connection.Close();
                }
            }
            return schemaTable;
        }

        public override Guid RunProcedureGUID(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            SqlCommand command = this.BuildQueryCommand(storedprocedureName, parameters, paramOutput);
            try
            {
                if (command.Connection.State == ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return Guid.Empty;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return (Guid)paramOutput.Value;
        }

        public override IDataReader RunProcedureReader(string storedprocedureName, IDataParameter[] parameters)
        {
            SqlCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            if (command.Connection.State == ConnectionState.Closed)
            {
                command.Connection.Open();
            }
            try
            {
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
                return null;
            }
        }
        public override int ExecuteNonQuery(string strSQL)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, object parameterevalue)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, int size, object parameterevalue)
        {
            throw new NotImplementedException();
        }
    }
}
