﻿

using System;
using System.Data;
using System.Web.Configuration;

namespace Intranet.Common
{

    public  abstract class IntranetDB
    {
        private string _connectionstring;

        public abstract DataRow Execute(string strSQL);
        public abstract DataSet Execute(string strSQL, string tablename);

        public static IntranetDB GetIntranetDB()
        {
            return new SqlDB(WebConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString);
        }

        public static IntranetDB GetIntranetDB(string connectionstring)
        {
            return new SqlDB(connectionstring);
        }

        public static IntranetDB GetIntranetDB(string connectionstring, string providertype)
        {
            if (providertype.ToUpper() == "SQL")
            {
                return new SqlDB(connectionstring);
            }
            return new OleDB(connectionstring);
        }

        public abstract IDataParameter GetParameter(string parametername, DataTypes parametertype);

        public abstract IDataParameter GetParameter(string parametername, DataTypes parametertype,
                                                    object parameterevalue);

        public abstract IDataParameter GetParameter(string parametername, DataTypes parametertype, int size,
                                                    object parameterevalue);

        public abstract bool RunProcedure(string storedprocedureName, IDataParameter[] parameters);

        public abstract int RunProcedure(string storedprocedurename, IDataParameter[] parameters,
                                         IDataParameter paramout);

        public abstract DataRow RunProcedureDR(string storedprocedureName, IDataParameter[] parameters);
        public abstract DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters);
        public abstract DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters, string tablename);
        public abstract DataTable RunProcedureDT(string storedprocedureName, IDataParameter[] parameters);

        public abstract Guid RunProcedureGUID(string storedprocedurename, IDataParameter[] parameters,
                                              IDataParameter paramout);

        public abstract IDataReader RunProcedureReader(string storedprocedureName, IDataParameter[] parameters);

        protected string ConnectionString
        {
            get { return this._connectionstring; }
            set { this._connectionstring = value; }
        }

        public abstract int ExecuteNonQuery(string strSQL);
        public abstract System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype);
        public abstract System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, object parameterevalue);
        public abstract System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, int size, object parameterevalue);        
    }
}
