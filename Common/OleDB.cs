﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace Intranet.Common
{
    public class OleDB : IntranetDB
    {
        protected OleDbConnection Connection;

        public OleDB(string connectionstring)
        {
            base.ConnectionString = connectionstring;
            this.Connection = new OleDbConnection(base.ConnectionString);
        }

        protected OleDbCommand BuildQueryCommand(string storedprocedureName, IDataParameter[] parameters)
        {
            OleDbCommand command = new OleDbCommand(storedprocedureName, this.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            foreach (OleDbParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        protected OleDbCommand BuildQueryCommand(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            OleDbCommand command = new OleDbCommand(storedprocedureName, this.Connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            foreach (OleDbParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            command.Parameters.Add(paramOutput);
            return command;
        }

        public override DataRow Execute(string strSQL)
        {
            DataSet set = this.Execute(strSQL, "temp");
            DataRow row = set.Tables["temp"].NewRow();
            if (set.Tables["temp"].Rows.Count > 0)
            {
                row = set.Tables["temp"].Rows[0];
            }
            return row;
        }

        public override DataSet Execute(string strSQL, string tablename)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter(strSQL, this.Connection);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet, tablename);
            return dataSet;
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype)
        {
            return new OleDbParameter(parametername, (OleDbType)parametertype);
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype, object parameterevalue)
        {
            return new OleDbParameter(parametername, (OleDbType)parametertype) { Value = parameterevalue };
        }

        public override IDataParameter GetParameter(string parametername, DataTypes parametertype, int size, object parameterevalue)
        {
            return new OleDbParameter(parametername, (OleDbType)parametertype, size) { Value = parameterevalue };
        }

        public OleDbType GetSQLDBType(DataTypes ptype)
        {
            if (ptype == DataTypes.bit)
            {
                return OleDbType.Boolean;
            }
            if (ptype == DataTypes.bigint)
            {
                return OleDbType.BigInt;
            }
            if (ptype == DataTypes.character)
            {
                return OleDbType.Char;
            }
            if (ptype == DataTypes.datetime)
            {
                return OleDbType.DBTimeStamp;
            }
            if (ptype == DataTypes.image)
            {
                return OleDbType.LongVarBinary;
            }
            if (ptype == DataTypes.integer)
            {
                return OleDbType.Integer;
            }
            if (ptype == DataTypes.nvarchar)
            {
                return OleDbType.VarWChar;
            }
            if (ptype == DataTypes.smallint)
            {
                return OleDbType.SmallInt;
            }
            if (ptype == DataTypes.varchar)
            {
                return OleDbType.VarChar;
            }
            if (ptype == DataTypes.ntext)
            {
                return OleDbType.LongVarWChar;
            }
            return OleDbType.Variant;
        }

        public override bool RunProcedure(string storedprocedureName, IDataParameter[] parameters)
        {
            OleDbCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return true;
        }

        public override int RunProcedure(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            OleDbCommand command = this.BuildQueryCommand(storedprocedureName, parameters, paramOutput);
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return (int)paramOutput.Value;
        }

        public override DataRow RunProcedureDR(string storedprocedureName, IDataParameter[] parameters)
        {
            DataSet set = this.RunProcedureDS(storedprocedureName, parameters);
            DataRow row = set.Tables[0].NewRow();
            if (set.Tables[0].Rows.Count > 0)
            {
                row = set.Tables[0].Rows[0];
            }
            return row;
        }

        public override DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter
            {
                SelectCommand = this.BuildQueryCommand(storedprocedureName, parameters)
            };
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet, "TableName");
            return dataSet;
        }

        public override DataSet RunProcedureDS(string storedprocedureName, IDataParameter[] parameters, string tablename)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter
            {
                SelectCommand = this.BuildQueryCommand(storedprocedureName, parameters)
            };
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet, tablename);
            return dataSet;
        }

        public override DataTable RunProcedureDT(string storedprocedureName, IDataParameter[] parameters)
        {
            OleDbCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            this.Connection.Open();
            OleDbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            DataTable schemaTable = reader.GetSchemaTable();
            while (reader.Read())
            {
                DataRow row = schemaTable.NewRow();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row[i] = reader[i];
                }
                schemaTable.Rows.Add(row);
            }
            reader.Close();
            return schemaTable;
        }

        public override Guid RunProcedureGUID(string storedprocedureName, IDataParameter[] parameters, IDataParameter paramOutput)
        {
            OleDbCommand command = this.BuildQueryCommand(storedprocedureName, parameters, paramOutput);
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch
            {
                return Guid.Empty;
            }
            finally
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
            }
            return (Guid)paramOutput.Value;
        }

        public override IDataReader RunProcedureReader(string storedprocedureName, IDataParameter[] parameters)
        {
            OleDbCommand command = this.BuildQueryCommand(storedprocedureName, parameters);
            if (command.Connection.State == ConnectionState.Closed)
            {
                command.Connection.Open();
            }
            try
            {
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch
            {
                if (command.Connection.State == ConnectionState.Open)
                {
                    command.Connection.Close();
                }
                return null;
            }
        }

        public override int ExecuteNonQuery(string strSQL)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, object parameterevalue)
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDataParameter GetParameter(string parametername, System.Data.DbType parametertype, int size, object parameterevalue)
        {
            throw new NotImplementedException();
        }
    }
}
