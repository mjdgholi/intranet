﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Common.ItcException
{
  public  class ItcApplicationErrorManagerException:Exception
    {
        public ItcApplicationErrorManagerException(string message): base(message)
        {
        }
    }
}
