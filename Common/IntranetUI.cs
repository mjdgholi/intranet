﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.Configuration.Settings;

namespace Intranet.Common
{
  public  class IntranetUI
    {

      public static string Convert2Persian(string strpass)
      {
          string str = string.Empty;
          if ((strpass != null) && (strpass != string.Empty))
          {
              char[] chArray = strpass.Replace("ك", "ک").ToCharArray();
              for (int i = 0; i < chArray.Length; i++)
              {
                  long num2 = (long)chArray[i];
                  if (num2 == 0x64aL)
                  {
                      chArray[i] = (char)int.Parse("1740");
                  }
                  str = str + chArray[i].ToString();
              }
          }
          return str;
      }

      public static string ConvertTo1256(string strpass)
      {
          char characterKaeOld = (char)1705;//ک
          char characterKaeNew = (char)1603;//ك
          char characteraeOld = (char)1740;//ی
          char characteraeNew = (char)1610;//ي
          strpass = (strpass.Replace(characterKaeOld, characterKaeNew));
          strpass = (strpass.Replace(characteraeOld, characteraeNew));
          return strpass;
      }

      public static string BuildUrl(string url)
      {
          if (url.StartsWith("~"))
          {
              return (PortalSettings.FullPortalPath + url.Substring(1));
          }
          return (PortalSettings.FullPortalPath + url);
      }

      public static int GetIDQueryString(string strqs, int retVal)
      {
          if ((strqs == null) || (strqs == ""))
          {
              return retVal;
          }
          try
          {
              return int.Parse(strqs);
          }
          catch
          {
              return retVal;
          }
      }

      public static string ShowDate(string datepass)
      {
          return ("<span dir='ltr'>" + datepass + "</span>");
      }

    }
}
