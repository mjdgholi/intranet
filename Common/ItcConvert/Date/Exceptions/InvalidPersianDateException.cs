﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Common.ItcConvert.Date.Exceptions
{
    public class InvalidPersianDateException : Exception
    {
        public InvalidPersianDateException()
            : base()
        {
        }

        public InvalidPersianDateException(string message)
            : base(message)
        {
        }
    }
}
