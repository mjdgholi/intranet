﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Common.ItcConvert.Date.Exceptions
{
    #region Localizer Base Class

    public abstract class BaseLocalizer
    {
        #region Abstract Methods

        public abstract string GetLocalizedString(StringID id);

        #endregion
    }

    #endregion
}
