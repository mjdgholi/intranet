﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Common
{
    public enum DataTypes
    {
        varchar,
        nvarchar,
        image,
        bit,
        character,
        integer,
        datetime,
        smallint,
        bigint,
        ntext
    }
}
