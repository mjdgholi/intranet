﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.Common.ItcConvert.Date;
using Intranet.Common.ItcException;

namespace Intranet.Common
{
    public class DConvert
    {
        public static string ShamsiToMiladi(string shamsiDate)
        {
            try
            { 
                if (shamsiDate == "//" | shamsiDate == "" | shamsiDate == null)
                    return System.DBNull.Value.ToString();

                return PersianDateConverter.ToGregorianDateTime(shamsiDate).ToString("yyyy/MM/dd");
            }
            catch (Exception exception)
            {
                var errortext = exception.Message; // ValidationError
                throw (new ItcApplicationErrorManagerException(errortext));
            }
        }

        public static string MiladiToShamsi(DateTime miladiDate)
        {
            try
            {
                return PersianDateConverter.ToPersianDate(miladiDate).ToString("yyyy/MM/dd");
            }
            catch (Exception exception)
            {
                var errortext = exception.Message; // ValidationError
                throw new ItcApplicationErrorManagerException(errortext);
            }
        }

        public static string MiladiToShamsi(string miladiDate)
        {
            try
            {
                return PersianDateConverter.ToPersianDate(miladiDate).ToString("yyyy/MM/dd");
            }
            catch (Exception exception)
            {
                var errortext = exception.Message; // ValidationError
                throw new ItcApplicationErrorManagerException(errortext);
            }
        }


    }
}

