﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Configuration
{
    public class Module
    {
        public bool AdminType;
        public ArrayList AuthLevels = new ArrayList();
        public int CacheTime;
        public string Container;
        public string DeskTopSRC;
        public string FriendlyName;
        public bool IsUpdatable;
        public int ModuleDefID;
        public int ModuleID;
        public int ModuleOrder;
        public string ModuleTitle;
        public string PaneName;
        public ArrayList Settings = new ArrayList();
        public string SkinHtml;
        public int TabID;
        public int UpdatePeriod;
    }
}
