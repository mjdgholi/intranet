﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.Configuration
{
    public class ModuleSetting
    {
        public string ControlType;
        public string DefaultValue;
        public string PossibleValues;
        public int SettingID;
        public string SettingName;
        public string SettingValue;
    }
}