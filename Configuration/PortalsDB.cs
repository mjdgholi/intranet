﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using Intranet.Common;
using Intranet.Configuration.Settings;
using Intranet.Security;

namespace Intranet.Configuration
{
    public class PortalsDB
    {
        //public static int AddPortal(string PortalName, string PortalPath, string PortalAlias)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalName", SqlDbType.NVarChar, 0x80), new SqlParameter("@PortalPath", SqlDbType.NVarChar, 0x80), new SqlParameter("@PortalAlias", SqlDbType.NVarChar, 0x80) };
        //    parameters[0].Value = PortalName;
        //    parameters[1].Value = PortalPath;
        //    parameters[2].Value = PortalAlias;
        //    SqlParameter paramout = new SqlParameter("@PortalID", SqlDbType.Int, 4)
        //    {
        //        Direction = ParameterDirection.Output
        //    };
        //    return intranetDB.RunProcedure("p_PortalAdd", parameters, paramout);
        //}

        //public static int AddPortalLang(int portalID, int langID, bool enabled, ref int homeTab)
        //{
        //    Language singleLanguage = ItemsDB.GetSingleLanguage(langID);
        //    homeTab = ItemsDB.AddTab(singleLanguage.LangHomeTabName, 1, 0, "", true, true, true, 0, 0, langID, portalID, "", "");
        //    UserDB.AddTabRole(15, homeTab);
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalID", SqlDbType.Int), new SqlParameter("@LangID", SqlDbType.Int), new SqlParameter("@Enabled", SqlDbType.Bit), new SqlParameter("@HomeTabID", SqlDbType.Int) };
        //    parameters[0].Value = portalID;
        //    parameters[1].Value = langID;
        //    parameters[2].Value = enabled;
        //    parameters[3].Value = (int)homeTab;
        //    SqlParameter paramout = new SqlParameter("@PortalLngID", SqlDbType.Int, 4)
        //    {
        //        Direction = ParameterDirection.Output
        //    };
        //    return intranetDB.RunProcedure("p_PortalLangAdd", parameters, paramout);
        //}

        //public static int CreatePortal(string portalName, string portalPath, string portalAlias, string firstName, string lastName, string username, string password, string email, int[] LangsID, Hashtable settings)
        //{
        //    int num;
        //    try
        //    {
        //        num = AddPortal(portalName, portalPath, portalAlias);
        //    }
        //    catch
        //    {
        //        return -1;
        //    }
        //    if (num > -1)
        //    {
        //        if (!CreatePortalPath(portalPath))
        //        {
        //            DeletePortal(portalAlias);
        //            return -2;
        //        }
        //        UpdatePortalSettings(num, "DefaultCulture", settings["DefaultCulture"].ToString());
        //        UpdatePortalSettings(num, "DefaultStyle", settings["DefaultStyle"].ToString());
        //        UpdatePortalSettings(num, "EmailAddress", settings["EmailAddress"].ToString());
        //        UpdatePortalSettings(num, "LoginAttempts", settings["LoginAttempts"].ToString());
        //        UpdatePortalSettings(num, "Monitoring", settings["Monitoring"].ToString());
        //        int roleID = 3;
        //        if (!PortalSettings.UseSingleUserBase)
        //        {
        //            int userID = UserDB.AddUser(username, firstName, lastName, email, FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5"), num, false);
        //            roleID = UserDB.AddRole("مدیر پورتال " + portalName, num);
        //            UserDB.AddUserRole(roleID, userID);
        //        }
        //        int homeTab = -1;
        //        for (int i = 0; i < LangsID.Length; i++)
        //        {
        //            int langID = LangsID[i];
        //            AddPortalLang(num, langID, true, ref homeTab);
        //            if (ItemsDB.GetSingleLanguage(langID).LangCulture == settings["DefaultCulture"].ToString())
        //            {
        //                int num7 = ItemsDB.AddModule(homeTab, 1, 3, "Login", "right");
        //                UserDB.AddModuleRole(15, num7, 4);
        //            }
        //            int num8 = ItemsDB.AddModule(homeTab, 1, 0x22, "Content", "content");
        //            UserDB.AddModuleRole(15, num8, 4);
        //        }
        //        int tabID = ItemsDB.AddTab("مدیریت سایت", 3, 0, "", true, false, false, 0, 0, 1, num, "", "");
        //        UserDB.AddTabRole(roleID, tabID);
        //        if (!PortalSettings.UseSingleUserBase)
        //        {
        //            int num10 = ItemsDB.AddTab("مدیریت کاربران", 1, tabID, "", true, false, true, 0, 0, 1, num, "", "");
        //            int num11 = ItemsDB.AddTab("مدیریت نقشها", 3, tabID, "", true, false, true, 0, 0, 1, num, "", "");
        //            UserDB.AddTabRole(roleID, num10);
        //            UserDB.AddTabRole(roleID, num11);
        //            int num12 = ItemsDB.AddModule(num10, 1, 7, "مدیریت کاربران", "content");
        //            UserDB.AddModuleRole(roleID, num12, 13);
        //            int num13 = ItemsDB.AddModule(num11, 3, 8, "مدیریت نقش ها", "content");
        //            UserDB.AddModuleRole(roleID, num13, 13);
        //        }
        //        int num14 = ItemsDB.AddTab("مدیریت صفحات", 5, tabID, "", true, false, true, 0, 0, 1, num, "", "");
        //        int num15 = ItemsDB.AddTab("مدیریت محتوای سایت", 7, tabID, "", true, false, true, 0, 0, 1, num, "", "");
        //        UserDB.AddTabRole(roleID, num14);
        //        UserDB.AddTabRole(roleID, num15);
        //        int moduleID = ItemsDB.AddModule(num14, 5, 9, "مدیریت صفحات", "content");
        //        UserDB.AddModuleRole(roleID, moduleID, 13);
        //        int num17 = ItemsDB.AddModule(num15, 7, 0x23, "مدیریت محتوای سایت", "content");
        //        UserDB.AddModuleRole(roleID, num17, 13);
        //    }
        //    return num;
        //}

        //private static bool CreatePortalPath(string portalPath)
        //{
        //    try
        //    {
        //        portalPath = portalPath.Replace("/", "");
        //        portalPath = portalPath.Replace(@"\", "");
        //        portalPath = portalPath.Replace(".", "");
        //        string str = (HttpContext.Current.Request.ApplicationPath == "/") ? "" : HttpContext.Current.Request.ApplicationPath;
        //        string path = HttpContext.Current.Server.MapPath(str + "/" + portalPath);
        //        if (!Directory.Exists(path))
        //        {
        //            Directory.CreateDirectory(path);
        //            File.Create(path + @"\Default.aspx");
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        //public static bool DeletePortal(string PortalAlias)
        //{
        //    DataRow singlePortal = GetSinglePortal(PortalAlias);
        //    if (((int)singlePortal["PortalID"]) == 0)
        //    {
        //        return false;
        //    }
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalAlias", SqlDbType.NVarChar, 0x80) };
        //    parameters[0].Value = PortalAlias;
        //    bool flag = intranetDB.RunProcedure("p_PortalDelete", parameters);
        //    string str = (HttpContext.Current.Request.ApplicationPath == "/") ? "" : HttpContext.Current.Request.ApplicationPath;
        //    string path = HttpContext.Current.Server.MapPath(str + "/" + singlePortal["PortalPath"].ToString());
        //    if (Directory.Exists(path))
        //    {
        //        Directory.Delete(path, true);
        //    }
        //    return flag;
        //}

        //public static bool DeletePortalLang(int PortalID, int LangID)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalID", SqlDbType.Int), new SqlParameter("@LangID", SqlDbType.Int) };
        //    parameters[0].Value = PortalID;
        //    parameters[1].Value = LangID;
        //    return intranetDB.RunProcedure("p_PortalLangDel", parameters);
        //}

        public static DataSet GetAllPortals()
        {
           DataSet cachedPortals = Caching.GetCachedPortals();
            if (cachedPortals == null)
            {
                cachedPortals = IntranetDB.GetIntranetDB().RunProcedureDS("p_PortalGetAll", new IDataParameter[0]);
              //  Caching.CachePortals(cachedPortals);
            }
            return cachedPortals;
        }

        public static DataSet GetAllPortalsSettings()
        {
            DataSet cachedPortalsSettings = Caching.GetCachedPortalsSettings();
            if (cachedPortalsSettings == null)
            {
                cachedPortalsSettings = IntranetDB.GetIntranetDB().RunProcedureDS("p_PortalSettingsGetAll", new IDataParameter[0]);
                //Caching.CachePortalsSettings(cachedPortalsSettings);
            }
            return cachedPortalsSettings;
        }

        //public static DataSet GetPagePortals(int PageSize, int CurrentPage, string WhereClause, string OrderBy)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    IDataParameter[] parameters = new IDataParameter[] { new SqlParameter("@PageSize", SqlDbType.Int), new SqlParameter("@CurrentPage", SqlDbType.Int), new SqlParameter("@WhereClause", SqlDbType.NVarChar, 0x3e8), new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100), new SqlParameter("@TableName", SqlDbType.NVarChar, 50), new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50) };
        //    parameters[0].Value = PageSize;
        //    parameters[1].Value = CurrentPage;
        //    parameters[2].Value = WhereClause;
        //    parameters[3].Value = OrderBy;
        //    parameters[4].Value = "t_Portals";
        //    parameters[5].Value = "PortalID";
        //    return intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
        //}

        //public static DataRow GetPortalLang(int portalID, int langID)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalID", SqlDbType.Int), new SqlParameter("@LangID", SqlDbType.Int) };
        //    parameters[0].Value = portalID;
        //    parameters[1].Value = langID;
        //    return intranetDB.RunProcedureDR("p_PortalLangGet", parameters);
        //}

        public static DataRow GetSinglePortal(string portalAlias)
        {
            DataView defaultView = GetAllPortals().Tables[0].DefaultView;
            defaultView.RowFilter = "PortalAlias = '" + portalAlias + "'";
            if (defaultView.Count > 0)
            {
                return defaultView[0].Row;
            }
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = new IDataParameter[] { intranetDB.GetParameter("@PortalAlias", DataTypes.nvarchar, 0x80, portalAlias) };
            return intranetDB.RunProcedureDR("p_PortalGetSingle", parameters);
        }

        //public static bool UpdatePortal(string PortalName, string PortalPath, string PortalAlias, string oldPortalAlias)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalName", SqlDbType.NVarChar, 0x80), new SqlParameter("@PortalPath", SqlDbType.NVarChar, 0x80), new SqlParameter("@PortalAlias", SqlDbType.NVarChar, 0x80), new SqlParameter("@OldPortalAlias", SqlDbType.NVarChar, 0x80) };
        //    parameters[0].Value = PortalName;
        //    parameters[1].Value = PortalPath;
        //    parameters[2].Value = PortalAlias;
        //    parameters[3].Value = oldPortalAlias;
        //    return intranetDB.RunProcedure("p_PortalsUpdate", parameters);
        //}

        //public static bool UpdatePortalSettings(int portalID, string settingName, string settingValue)
        //{
        //    IntranetDB intranetDB = IntranetDB.GetIntranetDB();
        //    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@PortalID", SqlDbType.Int), new SqlParameter("@SettingName", SqlDbType.NVarChar, 50), new SqlParameter("@SettingValue", SqlDbType.NVarChar, 0x5dc) };
        //    parameters[0].Value = portalID;
        //    parameters[1].Value = settingName;
        //    parameters[2].Value = settingValue;
        //    return intranetDB.RunProcedure("p_PortalSettingUpdate", parameters);
        //}
    }
}
