﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.Security;

namespace Intranet.Configuration
{
   public class ItemsDB
    {
       public static ArrayList GetModuleSettings(int moduleId)
       {
           ArrayList list = new ArrayList();
           DataRow[] rowArray = GetAllModulesSettings().Tables[0].Select(" ModuleID = " + moduleId);
           for (int i = 0; i < rowArray.Length; i++)
           {
               ModuleSetting setting = new ModuleSetting
               {
                   SettingID = (int)rowArray[i]["SettingID"],
                   SettingName = rowArray[i]["SettingName"].ToString(),
                   PossibleValues = rowArray[i]["SettingValues"].ToString(),
                   DefaultValue = rowArray[i]["DefValue"].ToString(),
                   SettingValue = rowArray[i]["SelfValue"].ToString()
               };
               if (setting.PossibleValues.Length == 0)
               {
                   setting.ControlType = "T";
               }
               else if (setting.PossibleValues == "yes;no")
               {
                   setting.ControlType = "C";
               }
               else
               {
                   setting.ControlType = "D";
               }
               list.Add(setting);
           }
           return list;
       }

       public static Module GetSingleModule(DataRow mydr)
       {
           Module module = new Module();
           if (mydr.Table.Rows.Count > 0)
           {
               module.ModuleID = int.Parse(mydr["ModuleID"].ToString());
               module.ModuleDefID = int.Parse(mydr["ModuleDefID"].ToString());
               module.ModuleOrder = int.Parse(mydr["ModuleOrder"].ToString());
               module.ModuleTitle = mydr["ModuleTitle"].ToString();
               module.PaneName = mydr["PaneName"].ToString();
               module.DeskTopSRC = mydr["DeskTopSRC"].ToString();
               module.TabID = (int)mydr["TabID"];
               module.Container = mydr["Container"].ToString();
               module.SkinHtml = mydr["SkinHtml"].ToString();
               module.IsUpdatable = (bool)mydr["Updatable"];
               module.UpdatePeriod = (int)mydr["UpdatePeriod"];
               module.CacheTime = Convert.ToInt32(mydr["CacheTime"]);
               module.AdminType = (bool)mydr["AdminType"];
               module.Settings = GetModuleSettings(module.ModuleID);
               ArrayList moduleRBAT = GetModuleRBAT(module.ModuleDefID);
               for (int i = 0; i < moduleRBAT.Count; i++)
               {
                   AuthTypeRoleBased based=new AuthTypeRoleBased();
                   based = new AuthTypeRoleBased
                   {
                       AuthID = ((AuthTypeRoleBased)moduleRBAT[i]).AuthID,
                       AuthName = ((AuthTypeRoleBased)moduleRBAT[i]).AuthName,
                       AuthorizedRoles = UserDB.GetModuleRolesSTR(module.ModuleID, based.AuthID)
                   };
                   module.AuthLevels.Add(based);
               }
           }
           return module;
       }

       public static Module GetSingleModule(int moduleID)
       {
           IntranetDB intranetDB = IntranetDB.GetIntranetDB();
           SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ModuleID", SqlDbType.Int, 4) };
           parameters[0].Value = moduleID;
           return GetSingleModule(intranetDB.RunProcedureDR("sp_GetSingleModule", parameters));
       }
       public static ArrayList GetModuleRBAT(int moduledefid)
       {
           ArrayList list = new ArrayList();
           DataRow[] rowArray = GetAllModulesRBAT().Tables[0].Select("ModuleDefID=" + moduledefid);
           for (int i = 0; i < rowArray.Length; i++)
           {
               AuthTypeRoleBased based = new AuthTypeRoleBased
               {
                   AuthID = int.Parse(rowArray[i]["AuthID"].ToString()),
                   AuthName = rowArray[i]["AuthName"].ToString()
               };
               list.Add(based);
           }
           return list;
       }

       public static DataSet GetAllModulesRBAT()
       {
           DataSet cachedModulesRBAT = Caching.GetCachedModulesRBAT();
           if (cachedModulesRBAT == null)
           {
               IntranetDB intranetDB = IntranetDB.GetIntranetDB();
               SqlParameter[] parameters = new SqlParameter[0];
               cachedModulesRBAT = intranetDB.RunProcedureDS("p_ModulesGetRBAT", parameters);
               //Caching.CacheModulesRBAT(cachedModulesRBAT);
           }
           return cachedModulesRBAT;
       }

       public static DataSet GetAllModulesSettings()
       {
           DataSet cachedModulesSettings = Caching.GetCachedModulesSettings();
           if (cachedModulesSettings == null)
           {
               IntranetDB intranetDB = IntranetDB.GetIntranetDB();
               SqlParameter[] parameters = new SqlParameter[0];
               cachedModulesSettings = intranetDB.RunProcedureDS("p_ModulesGetSettings", parameters);
              // Caching.CacheModulesSettings(cachedModulesSettings);
           }
           return cachedModulesSettings;
       }
    }
}
