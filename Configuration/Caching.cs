﻿namespace Intranet.Configuration
{
    using System;
    using System.Data;
    using System.Web;
    using System.Web.Caching;

    public class Caching
    {
        public static void CacheLanguages(DataSet languages)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_Language");
            HttpContext.Current.Cache.Insert("LanguagesCache", languages, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CacheModules(DataSet modules)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_Module");
            HttpContext.Current.Cache.Insert("ModulesCache", modules, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CacheModulesRBAT(DataSet modules)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_AuthRoleModuleDef");
            HttpContext.Current.Cache.Insert("ModulesRBATCache", modules, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CacheModulesRoles(DataSet modulesRoles)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_ModuleRole");
            HttpContext.Current.Cache.Insert("ModulesRolesCache", modulesRoles, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CacheModulesSettings(DataSet modules)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_ModuleSettings");
            HttpContext.Current.Cache.Insert("ModulesSettingsCache", modules, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CachePortals(DataSet portals)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_Portals");
            HttpContext.Current.Cache.Insert("PortalsCache", portals, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CachePortalsSettings(DataSet PortalsSettings)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_Portals");
            HttpContext.Current.Cache.Insert("PortalsSettingsCache", PortalsSettings, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void CacheTabs(DataSet tabs, int portalid, int langID)
        {
            SqlCacheDependency dependencies = new SqlCacheDependency("PortalDBCache", "t_Tab");
            HttpContext.Current.Cache.Insert(GetTabsCacheName(portalid, langID), tabs, dependencies, DateTime.Now.AddDays(1.0), Cache.NoSlidingExpiration);
        }

        public static void DeleteLanguagesCache()
        {
            HttpContext.Current.Cache.Remove("LanguagesCache");
        }

        public static void DeleteModulesCache()
        {
            HttpContext.Current.Cache.Remove("ModulesCache");
        }

        public static void DeleteModulesRBATCache()
        {
            HttpContext.Current.Cache.Remove("ModulesRBATCache");
        }

        public static void DeleteModulesRolesCache()
        {
            HttpContext.Current.Cache.Remove("ModulesRolesCache");
        }

        public static void DeleteModulesSettingsCache()
        {
            HttpContext.Current.Cache.Remove("ModulesSettingsCache");
        }

        public static void DeletePortalsCache()
        {
            HttpContext.Current.Cache.Remove("PortalsCache");
        }

        public static void DeletePortalsSettingdCache()
        {
            HttpContext.Current.Cache.Remove("PortalsSettingsCache");
        }

        public static DataSet GetCachedLanguages()
        {
            return (HttpContext.Current.Cache["LanguagesCache"] as DataSet);
        }

        public static DataSet GetCachedModules()
        {
            return (HttpContext.Current.Cache["ModulesCache"] as DataSet);
        }

        public static DataSet GetCachedModulesRBAT()
        {
            return (HttpContext.Current.Cache["ModulesRBATCache"] as DataSet);
        }

        public static DataSet GetCachedModulesRoles()
        {
            return (HttpContext.Current.Cache["ModulesRolesCache"] as DataSet);
        }

        public static DataSet GetCachedModulesSettings()
        {
            return (HttpContext.Current.Cache["ModulesSettingsCache"] as DataSet);
        }

        public static DataSet GetCachedPortals()
        {
            return (HttpContext.Current.Cache["PortalsCache"] as DataSet);
        }

        public static DataSet GetCachedPortalsSettings()
        {
            return (HttpContext.Current.Cache["PortalsSettingsCache"] as DataSet);
        }

        public static DataSet GetCachedTabs(int portalid, int langID)
        {
            return (HttpContext.Current.Cache[GetTabsCacheName(portalid, langID)] as DataSet);
        }

        public static string GetTabsCacheName(int portalid, int langID)
        {
            return string.Concat(new object[] { "PortalTabsDS_", portalid, "_", langID });
        }
    }
}

