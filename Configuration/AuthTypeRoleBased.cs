﻿namespace Intranet.Configuration
{
    using System;

    public class AuthTypeRoleBased
    {
        public int AuthID;
        public string AuthName;
        public string AuthorizedRoles;
    }
}

