﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Intranet.Common;

namespace Intranet.Configuration.Settings
{
    public class Portal
    {
        public static void AccessDenied()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                throw new HttpException(0x193, "Access Denied", 2);
            }
            HttpContext.Current.Response.Redirect(IntranetUI.BuildUrl("~/accessdenied.aspx"));
        }
    }
}