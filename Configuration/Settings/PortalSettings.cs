﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Intranet.Common;

namespace Intranet.Configuration.Settings
{
  public  class PortalSettings
    {
        public string _portalAlias;
        public static bool isCMS = false;
        public static bool isMultiPortal = true;
        public int portalID;
        public string portalName;
        public string portalPath;
        public Hashtable settings;

        public PortalSettings(string portalAlias)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameterArray = new SqlParameter[] { new SqlParameter("@PortalAlias", SqlDbType.NVarChar, 0x80) };
            parameterArray[0].Value = portalAlias;
            DataRow singlePortal = PortalsDB.GetSinglePortal(portalAlias);
            if (singlePortal.Table.Rows.Count > 0)
            {
                this.portalName = singlePortal["PortalName"].ToString();
                this.portalID = (int)singlePortal["PortalID"];
                this._portalAlias = singlePortal["PortalAlias"].ToString();
                this.portalPath = singlePortal["PortalPath"].ToString();
            }
            this.settings = this.GetPortalSettings(this.portalID);
        }

        public Hashtable GetPortalSettings(int pID)
        {
            Hashtable hashtable = new Hashtable();
            DataView defaultView = PortalsDB.GetAllPortalsSettings().Tables[0].DefaultView;
            defaultView.RowFilter = "PortalID=" + this.portalID;
            for (int i = 0; i < defaultView.Count; i++)
            {
                hashtable[defaultView[i]["SettingName"].ToString()] = defaultView[i]["SettingValue"].ToString();
            }
            return hashtable;
        }

        public static string BoxEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["BoxEmail"];
            }
        }

        public static string CardEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["CardEmail"];
            }
        }

        public static string DefaultCulture
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.settings["DefaultCulture"].ToString();
            }
        }

        public static string DefaultPortal
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultPortal"];
            }
        }

        public static string DefaultStyle
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.settings["DefaultStyle"].ToString();
            }
        }

        public static string EmailAddress
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.settings["EmailAddress"].ToString();
            }
        }

        public static string FullPortalPath
        {
            get
            {
                return (PortalPath + ((LocalPortalPath == "") ? "" : ("/" + LocalPortalPath)));
            }
        }

        public static string LocalPortalPath
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.portalPath;
            }
        }

        public static int LoginAttempts
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return int.Parse(settings.settings["LoginAttempts"].ToString());
            }
        }

        public static string MeetingEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["MeetingEmail"];
            }
        }

        public static bool Monitoring
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return (settings.settings["Monitoring"].ToString() == "on");
            }
        }

        public string portalAlias
        {
            get
            {
                return this._portalAlias;
            }
        }

        public static string PortalAlias
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings._portalAlias;
            }
        }

        public static int PortalID
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.portalID;
            }
        }

        public static string PortalName
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.portalName;
            }
        }

        public static string PortalPath
        {
            get
            {
                return ConfigurationManager.AppSettings["PortalPath"];
            }
        }

        public static Hashtable Settings
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.settings;
            }
        }

        public static string SMTPServer
        {
            get
            {
                PortalSettings settings = (PortalSettings)HttpContext.Current.Items["PortalSettings"];
                return settings.settings["SMTPServer"].ToString();
            }
        }

        public static SqlConnection SqlconstrFish
        {
            get
            {
                return new SqlConnection(ConfigurationManager.AppSettings["SqlconstrFish"]);
            }
        }

        public static SqlConnection SqlconstrSelf
        {
            get
            {
                return new SqlConnection(WebConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString);
            }
        }

        public static bool UseSingleUserBase
        {
            get
            {
                return bool.Parse(ConfigurationManager.AppSettings["UseSingleUserBase"]);
            }
        }
    }
}
