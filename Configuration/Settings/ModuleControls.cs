﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Intranet.Configuration.Settings
{
    public class ModuleControls : UserControl
    {
        private Module _moduleConfiguration;
        private int moduleID;

        public Module ModuleConfiguration
        {
            get { return this._moduleConfiguration; }
            set { this._moduleConfiguration = value; }
        }

        public int ModuleId
        {
            get { return (int.Parse(HttpContext.Current.Request["mID"].ToString())); }
            set {}
        }


    }
}
