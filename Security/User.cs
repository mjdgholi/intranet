﻿namespace Intranet.Security
{
    using Intranet.Configuration.Settings;
    using System;

    public class User
    {
        private string email;
        private string fname;
        private bool isLocked;
        private bool isSuperUser;
        private string lname;
        private string pass;
        private string phone;
        private int portalID;
        private int userid;
        private string username;
        private string userstyle;

        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
            }
        }

        public string FirstName
        {
            get
            {
                return this.fname;
            }
            set
            {
                this.fname = value;
            }
        }

        public bool IsLocked
        {
            get
            {
                return this.isLocked;
            }
            set
            {
                this.isLocked = value;
            }
        }

        public bool IsSuperUser
        {
            get
            {
                return this.isSuperUser;
            }
            set
            {
                this.isSuperUser = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lname;
            }
            set
            {
                this.lname = value;
            }
        }

        public string Password
        {
            get
            {
                return this.pass;
            }
            set
            {
                this.pass = value;
            }
        }

        public string PhoneNo
        {
            get
            {
                return this.phone;
            }
            set
            {
                this.phone = value;
            }
        }

        public int PortalID
        {
            get
            {
                return this.portalID;
            }
            set
            {
                this.portalID = value;
            }
        }

        public int UserID
        {
            get
            {
                return this.userid;
            }
            set
            {
                this.userid = value;
            }
        }

        public string UserName
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }

        public string UserStyle
        {
            get
            {
                if ((this.userstyle == null) || (this.userstyle == ""))
                {
                    return PortalSettings.DefaultStyle;
                }
                return this.userstyle;
            }
            set
            {
                this.userstyle = value;
            }
        }
    }
}

