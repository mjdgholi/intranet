﻿namespace Intranet.Security
{
    using Intranet.Common;
    using System;
    using System.Data;
    using System.Data.SqlClient;

    public class ForgetPassDB
    {
        public static int AddNewRequest(int userid, DateTime datesent)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID_1", SqlDbType.Int, 4), new SqlParameter("@DateSent_2", SqlDbType.DateTime) };
            parameters[0].Value = userid;
            parameters[1].Value = datesent;
            SqlParameter paramout = new SqlParameter("@ID_3", SqlDbType.Int) {
                Direction = ParameterDirection.Output
            };
            return intranetDB.RunProcedure("sp_ForgetPassInsert", parameters, paramout);
        }

        public static bool DeleteRequest(int id)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ID_1", SqlDbType.Int, 4) };
            parameters[0].Value = id;
            return intranetDB.RunProcedure("sp_ForgetPassDel", parameters);
        }

        public static DataRow GetSingleRequest(Guid reqid)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ReqID", SqlDbType.UniqueIdentifier) };
            parameters[0].Value = reqid;
            return intranetDB.RunProcedureDR("sp_ForgetPassGet", parameters);
        }

        public static DataRow GetSingleRequestByID(int id)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ID", SqlDbType.Int) };
            parameters[0].Value = id;
            return intranetDB.RunProcedureDR("sp_ForgetPassGetbyID", parameters);
        }
    }
}

