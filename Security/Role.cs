﻿namespace Intranet.Security
{
    using System;
    using System.Web;

    public class Role
    {
        private int roleid;
        private string rolename;

        public static bool IsInRole(string role)
        {
            HttpContext current = HttpContext.Current;
            return (((role != "") && (role != null)) && (((role == "15") || (current.Request.IsAuthenticated && (role == "13"))) || current.User.IsInRole(role)));
        }

        public static bool IsInRoles(string roles)
        {
            HttpContext current = HttpContext.Current;
            if (((roles != null) && (roles != string.Empty)) && (roles.Trim() != ""))
            {
                foreach (string str in roles.Split(new char[] { ';' }))
                {
                    if (((str != "") && (str != null)) && ((((str == "15") || (current.Request.IsAuthenticated && (str == "13"))) || (!current.Request.IsAuthenticated && (str == "14"))) || current.User.IsInRole(str)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public int RoleID
        {
            get
            {
                return this.roleid;
            }
            set
            {
                this.roleid = value;
            }
        }

        public string RoleName
        {
            get
            {
                return this.rolename;
            }
            set
            {
                this.rolename = value;
            }
        }
    }
}

