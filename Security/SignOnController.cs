﻿namespace Intranet.Security
{
    using Intranet.Common;
    using Intranet.Configuration.Settings;
    using System;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;

    public class SignOnController
    {
        //*_*//public static void ExtendCookie(Portal portalSettings)
        //{
        //    int minuteAdd = 60;
        //    ExtendCookie(portalSettings, minuteAdd);
        //}

        //*_*//public static void ExtendCookie(Portal portalSettings, int minuteAdd)
        //{
        //    DateTime now = DateTime.Now;
        //    TimeSpan span = new TimeSpan(0, 0, minuteAdd, 0, 0);
        //    string str = !PortalSettings.UseSingleUserBase ? ("PalizPortal_" + PortalSettings.PortalAlias.ToLower()) : "PalizPortal";
        //    HttpContext.Current.Response.Cookies[str].Expires = now.Add(span);
        //}

        public static void KillSession()
        {
            SignOut(IntranetUI.BuildUrl("~/Admin/Logon.aspx"), true);
        }

        public static User SignOn(string user, string password, bool persistent)
        {
            return SignOn(user, password, persistent, null);
        }

        public static User SignOn(string login, string password, bool persistent, string redirectPage)
        {
            User user = UserDB.Login(login, FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5"), PortalSettings.PortalID);
            if ((user != null) && (user.UserID != -2))
            {
                //   if (PortalSettings.Monitoring)
                //{
                //    try
                //    {
                //         Monitoring.AddMonitoring(user.UserID, PortalSettings.PortalID, -1, "Logon");
                //    }
                //    catch (Exception exception)
                //    {
                //        LogException.HandleException(exception);
                //    }
                //}

                FormsAuthentication.SetAuthCookie(user.UserID.ToString(), persistent);
                string str = !PortalSettings.UseSingleUserBase ? ("PalizPortal_" + PortalSettings.PortalAlias.ToLower()) : "PalizPortal";
                int minutes = 120;
                HttpCookie cookie = HttpContext.Current.Response.Cookies[str];
                cookie.Value = user.UserID.ToString();
                cookie.Path = "/";
                if (persistent)
                {
                    cookie.Expires = DateTime.Now.AddYears(50);
                }
                else
                {
                    DateTime now = DateTime.Now;
                    TimeSpan span = new TimeSpan(0, 0, minutes, 0, 0);
                    cookie.Expires = DateTime.Now.AddMinutes((double)minutes);
                }
                string[] roles = UserDB.GetRoles(user.UserID);
                string userData = "";
                foreach (string str3 in roles)
                {
                    userData = userData + str3 + ";";
                }
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.UserID.ToString(), DateTime.Now, DateTime.Now.AddMinutes((double) minutes), false, userData);
                cookie.Value = FormsAuthentication.Encrypt(ticket);                
                HttpContext current = HttpContext.Current;
                current.User = new GenericPrincipal(new FormsIdentity(ticket), roles);
                
                if (string.IsNullOrEmpty(redirectPage))
                {
                    if (current.Request.UrlReferrer != null)
                    {
                        current.Response.Redirect(current.Request.UrlReferrer.ToString());
                        return user;
                    }
                    current.Response.Redirect(IntranetUI.BuildUrl("~/default.aspx"));
                    return user;
                }
                current.Response.Redirect(redirectPage);
            }
            return user;
        }

        public static void SignOut()
        {
            SignOut(IntranetUI.BuildUrl("~/Default.aspx"), true);
        }

        public static void SignOut(string urlToRedirect, bool removeLogin)
        {
            HttpContext current = HttpContext.Current;
            if (current.User != null)
            {
              //*_*//  Monitoring.AddMonitoring(Convert.ToInt32(current.User.Identity.Name), PortalSettings.PortalID, -1, "LogOut");
            }
            FormsAuthentication.SignOut();
            if (removeLogin)
            {
                if (PortalSettings.UseSingleUserBase)
                {
                    HttpCookie cookie = current.Response.Cookies["PalizPortal"];
                    if (cookie != null)
                    {
                        cookie.Value = null;
                        cookie.Expires = new DateTime(0x7cf, 10, 12);
                        cookie.Path = "/";
                    }
                }
                else
                {
                    HttpCookie cookie2 = current.Response.Cookies["PalizPortal_" + PortalSettings.PortalAlias.ToLower()];
                    if (cookie2 != null)
                    {
                        cookie2.Value = null;
                        cookie2.Expires = new DateTime(0x7cf, 10, 12);
                        cookie2.Path = "/";
                    }
                }
            }
            if (urlToRedirect.Length > 0)
            {
                current.Response.Redirect(urlToRedirect);
            }
        }
    }
}

