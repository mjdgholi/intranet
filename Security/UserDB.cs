﻿namespace Intranet.Security
{
    using Intranet.Common;
    using Intranet.Configuration;
    using Intranet.Configuration.Settings;
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web;

    public class UserDB
    {
        public static int AddModuleRole(int roleID, int moduleID, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_AddModuleRole", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int, 4) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ModuleID", SqlDbType.Int, 4) {
                Value = moduleID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@ModuleRoleID", SqlDbType.Int, 4) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter4);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
                Caching.DeleteModulesRolesCache();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter4.Value;
        }

        public static int AddObjRole(int roleID, int objID, string objName, int authID, int moduleID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_ObjAddRole", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int, 4) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@ModuleID", SqlDbType.Int, 4);
            if (moduleID != 0)
            {
                parameter4.Value = moduleID;
            }
            else
            {
                parameter4.Value = DBNull.Value;
            }
            command.Parameters.Add(parameter4);
            SqlParameter parameter5 = new SqlParameter("@ObjRoleID", SqlDbType.Int, 4) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter5);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter5.Value;
        }

        public static int AddObjUser(int userID, int objID, string objName, int authID, int moduleID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_ObjAddUser", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int, 4) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@ModuleID", SqlDbType.Int, 4) {
                Value = moduleID
            };
            command.Parameters.Add(parameter4);
            SqlParameter parameter5 = new SqlParameter("@ObjUserID", SqlDbType.Int, 4) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter5);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter5.Value;
        }

        public static string AddRequest(string userName, string firstName, string lastName, string email, string phoneno, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserRegRequestAdd", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserName", SqlDbType.VarChar, 50) {
                Value = userName
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@FirstName", SqlDbType.NVarChar, 50) {
                Value = firstName
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@LastName", SqlDbType.NVarChar, 50) {
                Value = lastName
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@Email", SqlDbType.NVarChar, 100) {
                Value = email
            };
            command.Parameters.Add(parameter4);
            SqlParameter parameter5 = new SqlParameter("@PhoneNo", SqlDbType.NVarChar, 10) {
                Value = phoneno
            };
            command.Parameters.Add(parameter5);
            SqlParameter parameter6 = new SqlParameter("@PortalID", SqlDbType.Int) {
                Value = portalID
            };
            command.Parameters.Add(parameter6);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return "-1";
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return "0";
        }

        public static int AddRole(string roleName, int portalID)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleName", SqlDbType.NVarChar, 50), new SqlParameter("@PortalID", SqlDbType.Int) };
            parameters[0].Value = IntranetUI.Convert2Persian(roleName).Trim();
            parameters[1].Value = portalID;
            SqlParameter paramout = new SqlParameter("@RoleID", SqlDbType.Int) {
                Direction = ParameterDirection.Output
            };
            return intranetDB.RunProcedure("p_RoleAdd", parameters, paramout);
        }

        public static int AddTabRole(int roleID, int tabID)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleID", SqlDbType.Int, 4), new SqlParameter("@TabID", SqlDbType.Int, 4) };
            parameters[0].Value = roleID;
            parameters[1].Value = tabID;
            SqlParameter paramout = new SqlParameter("@TabRoleID", SqlDbType.Int) {
                Direction = ParameterDirection.Output
            };
            return intranetDB.RunProcedure("p_TabRoleAdd", parameters, paramout);
        }

        public static int AddUser(string userName, string firstName, string lastName, string email, string password, int portalID, bool isSuperUser)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserAdd", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@UserName", SqlDbType.VarChar, 50) {
                Value = userName
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@FirstName", SqlDbType.NVarChar, 50) {
                Value = firstName
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@LastName", SqlDbType.NVarChar, 50) {
                Value = lastName
            };
            command.Parameters.Add(parameter4);
            SqlParameter parameter5 = new SqlParameter("@Email", SqlDbType.NVarChar, 100) {
                Value = email
            };
            command.Parameters.Add(parameter5);
            SqlParameter parameter6 = new SqlParameter("@Password", SqlDbType.NVarChar, 100) {
                Value = password
            };
            command.Parameters.Add(parameter6);
            SqlParameter parameter7 = new SqlParameter("@PortalID", SqlDbType.Int) {
                Value = portalID
            };
            command.Parameters.Add(parameter7);
            SqlParameter parameter8 = new SqlParameter("@IsSuperUser", SqlDbType.Bit) {
                Value = isSuperUser
            };
            command.Parameters.Add(parameter8);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter.Value;
        }

        public static int AddUserRole(int roleID, int userID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserRoleAdd", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int, 4) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@UserRoleID", SqlDbType.Int, 4) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter3);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter3.Value;
        }

        public static int DeleteModuleRole(int moduleID, int roleID, int authid)
        {
            int num = 0;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_DeleteModuleRole", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ModuleID", SqlDbType.Int, 4) {
                Value = moduleID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authid
            };
            command.Parameters.Add(parameter3);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
               Caching.DeleteModulesRolesCache();
            }
            catch
            {
                num = -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return num;
        }

        public static bool DeleteObjRole(int roleID, int objID, string objName, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_ObjDeleteRole", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int, 4) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            command.Parameters.Add(parameter3);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return true;
        }

        public static bool DeleteObjUser(int userID, int objID, string objName, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_ObjDeleteUser", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int, 4) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            command.Parameters.Add(parameter3);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return true;
        }

        public static bool DeleteRequest(int ReqID)
        {
            bool flag = true;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserRegRequestDelete", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@ReqID", SqlDbType.Int) {
                Value = ReqID
            };
            command.Parameters.Add(parameter);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                flag = false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return flag;
        }

        public static bool DeleteRole(int roleID)
        {
            if (((roleID != 13) && (roleID != 15)) && (roleID != 14))
            {
                IntranetDB intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleID", SqlDbType.Int, 4) };
                parameters[0].Value = roleID;
                return intranetDB.RunProcedure("p_RoleDelete", parameters);
            }
            return false;
        }

        public static bool DeleteTabRole(int tabID, int roleID)
        {
            bool flag = true;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_TabRoleDelete", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@TabID", SqlDbType.Int, 4) {
                Value = tabID
            };
            command.Parameters.Add(parameter2);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                flag = false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return flag;
        }

        public static bool DeleteUser(int userID)
        {
            bool flag = true;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserDelete", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                flag = false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return flag;
        }

        public static bool DeleteUserRole(int userID, int roleID)
        {
            bool flag = true;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserRoleDelete", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter2);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                flag = false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return flag;
        }

        public static bool DeleteUserRolebyID(int userroleID)
        {
            bool flag = true;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("rb_DeleteUserRolebyID", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserRoleID", SqlDbType.Int) {
                Value = userroleID
            };
            command.Parameters.Add(parameter);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                flag = false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return flag;
        }

        public static DataSet GetAllModulesRoles()
        {
            DataSet cachedModulesRoles = Caching.GetCachedModulesRoles();
            if (cachedModulesRoles == null)
            {
                IntranetDB intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = new SqlParameter[0];
                cachedModulesRoles = intranetDB.RunProcedureDS("p_ModulesGetRoles", parameters);
                Caching.CacheModulesRoles(cachedModulesRoles);
            }
            return cachedModulesRoles;
        }

        public static DataSet GetAllRegResquests(string userName, string fName, string lName, string email, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserName", SqlDbType.VarChar, 50), new SqlParameter("@FirstName", SqlDbType.NVarChar, 50), new SqlParameter("@LastName", SqlDbType.NVarChar, 50), new SqlParameter("@Email", SqlDbType.NVarChar, 100), new SqlParameter("@PortalID", SqlDbType.Int) };
            parameters[0].Value = userName;
            parameters[1].Value = fName;
            parameters[2].Value = lName;
            parameters[3].Value = email;
            parameters[4].Value = portalID;
            return intranetDB.RunProcedureDS("p_UserRegResquestGetAll", parameters, "requests");
        }

        public static DataSet GetAllRoles(string roleName, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleName", SqlDbType.NVarChar, 50), new SqlParameter("@PortalID", SqlDbType.Int) };
            parameters[0].Value = roleName;
            parameters[1].Value = portalID;
            return intranetDB.RunProcedureDS("p_RoleGetAll", parameters, "Roles");
        }

        public static DataSet GetAllUsers(string userName, string fName, string lName, string email, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserName", SqlDbType.VarChar, 50), new SqlParameter("@FirstName", SqlDbType.NVarChar, 50), new SqlParameter("@LastName", SqlDbType.NVarChar, 50), new SqlParameter("@Email", SqlDbType.NVarChar, 100), new SqlParameter("@PortalID", SqlDbType.Int) };
            parameters[0].Value = userName;
            parameters[1].Value = fName;
            parameters[2].Value = lName;
            parameters[3].Value = email;
            parameters[4].Value = portalID;
            return intranetDB.RunProcedureDS("p_UserGetAll", parameters, "Users");
        }

        public static DataSet GetBelongaleRoles(int userID, int PortalID)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.Int), new SqlParameter("@PortalID", SqlDbType.Int) };
            parameters[0].Value = userID;
            parameters[1].Value = PortalID;
            return intranetDB.RunProcedureDS("sp_GetBelongableRoles", parameters);
        }

        public static DataSet GetBlngRolesTabs(int tabID, int searchID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetBlngRolesTabs", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@TabID", SqlDbType.Int, 4) {
                Value = tabID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@SearchID", SqlDbType.Int, 4) {
                Value = searchID
            };
            adapter.SelectCommand.Parameters.Add(parameter2);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "BelongableRoles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static DataSet GetModuleRoles(int moduleID, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetRolesByModule", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@ModuleID", SqlDbType.Int, 4) {
                Value = moduleID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            adapter.SelectCommand.Parameters.Add(parameter2);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "ModuleRoles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static DataSet GetModuleRolesAll(int moduleID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetRolesByModuleAll", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@ModuleID", SqlDbType.Int, 4) {
                Value = moduleID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "AllModuleRoles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static string GetModuleRolesSTR(int moduleID, int authtype)
        {
            DataRow[] rowArray = GetAllModulesRoles().Tables[0].Select(string.Concat(new object[] { "ModuleID=", moduleID, " AND AuthID=", authtype }));
            string str = string.Empty;
            for (int i = 0; i < rowArray.Length; i++)
            {
                str = str + rowArray[i]["RoleID"].ToString() + ";";
            }
            return str;
        }

        public static DataSet GetobjRoles(int objID, string objName, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetRolesByObj", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            adapter.SelectCommand.Parameters.Add(parameter2);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, objName + "Roles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static string GetObjRolesSTR(int objID, string objName, int authtype)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_GetRolesByObj", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authtype
            };
            command.Parameters.Add(parameter2);
            string str = string.Empty;
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    str = str + reader["RoleID"].ToString();
                    str = str + ";";
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return str;
        }

        public static DataSet GetobjUsers(int objID, string objName, int authID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetUsersByObj", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@ObjID", SqlDbType.Int, 4) {
                Value = objID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@AuthID", SqlDbType.Int, 4) {
                Value = authID
            };
            adapter.SelectCommand.Parameters.Add(parameter2);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, objName + "Users");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static DataSet GetRoleMembers(int roleID, string userName, string fName, string lName, int othroleID, int searchType, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleID", SqlDbType.Int, 4), new SqlParameter("@UserName", SqlDbType.VarChar, 50), new SqlParameter("@FirstName", SqlDbType.NVarChar, 50), new SqlParameter("@LastName", SqlDbType.NVarChar, 50), new SqlParameter("@othRoleID", SqlDbType.Int, 4), new SqlParameter("@SearchType", SqlDbType.Bit), new SqlParameter("@PortalID", SqlDbType.Int, 4) };
            parameters[0].Value = roleID;
            parameters[1].Value = userName;
            parameters[2].Value = fName;
            parameters[3].Value = lName;
            parameters[4].Value = othroleID;
            parameters[5].Value = searchType;
            parameters[6].Value = portalID;
            return intranetDB.RunProcedureDS("sp_GetRoleMembers", parameters, "RoleMembers");
        }

        public static string[] GetRoles(int userID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_GetRolesByUser", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            ArrayList list = new ArrayList();
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    list.Add(reader["RoleID"].ToString());
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public static User GetSingleRequest(int ReqID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserGetSingleRegRequest", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@ReqID", SqlDbType.Int) {
                Value = ReqID
            };
            command.Parameters.Add(parameter);
            User user = new User();
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    user.Email = reader["Email"].ToString();
                    user.UserName = reader["UserName"].ToString();
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    user.PhoneNo = reader["PhoneNo"].ToString();
                    user.PortalID = (int) reader["PortalID"];
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return user;
        }

        public static string GetSingleRole(int roleID)
        {
            string str = string.Empty;
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_GetSingleRole", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@RoleID", SqlDbType.Int, 4) {
                Value = roleID
            };
            command.Parameters.Add(parameter);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    str = reader["Role_Name"].ToString();
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return str;
        }

        public static User GetSingleUser(int userID)
        {
            User user = new User();
            try
            {
                if ((HttpContext.Current.Session["CurrentUser"] != null) && (((User) HttpContext.Current.Session["CurrentUser"]).UserID == userID))
                {
                    return (User) HttpContext.Current.Session["CurrentUser"];
                }
                IntranetDB intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.Int) };
                parameters[0].Value = userID;
                SqlDataReader reader = (SqlDataReader) intranetDB.RunProcedureReader("p_UserGetSingle", parameters);
                if (reader.Read())
                {
                    user.Email = reader["Email"].ToString();
                    user.UserID = userID;
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    user.Password = reader["UserPass"].ToString();
                    user.UserStyle = reader["UserStyle"].ToString();
                    user.UserName = reader["UserName"].ToString();
                    user.IsLocked = (bool) reader["IsLocked"];
                    user.IsSuperUser = (bool) reader["IsSuperUser"];
                    user.PortalID = (int) reader["PortalID"];
                }
                reader.Close();
            }
            catch
            {
                return user;
            }
            return user;
        }

        public static User GetSingleUserByEmail(string email)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserGetSingleByEmail", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@Email", SqlDbType.VarChar, 50) {
                Value = email
            };
            command.Parameters.Add(parameter);
            User user = new User {
                UserID = -1
            };
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    user.Email = reader["Email"].ToString();
                    user.UserID = (int) reader["UserID"];
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    user.Password = reader["UserPass"].ToString();
                    user.UserStyle = reader["UserStyle"].ToString();
                    user.UserName = reader["UserName"].ToString();
                    user.IsLocked = (bool) reader["IsLocked"];
                    user.IsSuperUser = (bool) reader["IsSuperUser"];
                    user.PortalID = (int) reader["PortalID"];
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return user;
        }

        public static User GetSingleUserByName(string userName)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserGetSingleByName", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserName", SqlDbType.VarChar, 50) {
                Value = userName
            };
            command.Parameters.Add(parameter);
            User user = new User {
                UserID = -1
            };
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    user.Email = reader["Email"].ToString();
                    user.UserID = (int) reader["UserID"];
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    user.Password = reader["UserPass"].ToString();
                    user.UserStyle = reader["UserStyle"].ToString();
                    user.UserName = reader["UserName"].ToString();
                    user.IsLocked = (bool) reader["IsLocked"];
                    user.IsSuperUser = (bool) reader["IsSuperUser"];
                    user.PortalID = (int) reader["PortalID"];
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return user;
        }

        public static DataSet GetTabRoles(int tabID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetRolesByTab", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@TabID", SqlDbType.Int, 4) {
                Value = tabID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "TabRoles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static DataSet GetUserRoles(int userID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlDataAdapter adapter = new SqlDataAdapter("sp_GetRolesByUser", sqlconstrSelf) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            adapter.SelectCommand.Parameters.Add(parameter);
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet, "UserRoles");
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return dataSet;
        }

        public static bool LockUser(int userid)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.Int) };
            parameters[0].Value = userid;
            return intranetDB.RunProcedure("p_UserLock", parameters);
        }

        public static User Login(string uID, string password, int portalID)
        {
            if (PortalSettings.UseSingleUserBase)
            {
                portalID = 0;
            }
            User user = new User();
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserLogin", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@uID", SqlDbType.VarChar, 50) {
                Value = uID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@Password", SqlDbType.NVarChar, 100) {
                Value = password
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@UserID", SqlDbType.Int) {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@PortalID", SqlDbType.Int) {
                Value = portalID
            };
            command.Parameters.Add(parameter4);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
                sqlconstrSelf.Close();
                if (((parameter3.Value != null) && (parameter3.Value != DBNull.Value)) && (((int) parameter3.Value) != -2))
                {
                    return GetSingleUser((int) parameter3.Value);
                }
                if (((parameter3.Value != null) && (parameter3.Value != DBNull.Value)) && (((int) parameter3.Value) == -2))
                {
                    user.UserID = -2;
                    return user;
                }
                user = null;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return user;
        }

        public static string SetTabRolesToSTR(int tabID)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("sp_GetRolesByTab", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@TabID", SqlDbType.Int, 4) {
                Value = tabID
            };
            command.Parameters.Add(parameter);
            string str = string.Empty;
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    str = str + reader["RoleID"].ToString();
                    str = str + ";";
                }
                reader.Close();
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return str;
        }

        public static bool UnLockUser(int userid)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.Int) };
            parameters[0].Value = userid;
            return intranetDB.RunProcedure("p_UserUnLock", parameters);
        }

        public static int UpdateRequestInfo(int ReqID, string userName, string firstName, string lastName, string email)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_RegRequestUpdate", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@ReqID", SqlDbType.Int) {
                Value = ReqID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@UserName", SqlDbType.VarChar, 50) {
                Value = userName
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@fName", SqlDbType.NVarChar, 50) {
                Value = firstName
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@lName", SqlDbType.NVarChar, 50) {
                Value = lastName
            };
            command.Parameters.Add(parameter4);
            SqlParameter parameter5 = new SqlParameter("@Email", SqlDbType.NVarChar, 100) {
                Value = email
            };
            command.Parameters.Add(parameter5);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter.Value;
        }

        public static bool UpdateRoleInfo(int roleID, string roleName)
        {
            if (((roleID != 3) && (roleID != 13)) && (roleID != 15))
            {
                IntranetDB intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@RoleName", SqlDbType.NVarChar, 50), new SqlParameter("@RoleID", SqlDbType.Int) };
                parameters[0].Value = IntranetUI.Convert2Persian(roleName).Trim();
                parameters[1].Value = roleID;
                return intranetDB.RunProcedure("p_RoleUpdateInfo", parameters);
            }
            return false;
        }

        public static int UpdateUserInfo(int userID, string firstName, string lastName, string email)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserUpdateInfo", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@fName", SqlDbType.NVarChar, 50) {
                Value = firstName
            };
            command.Parameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@lName", SqlDbType.NVarChar, 50) {
                Value = lastName
            };
            command.Parameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@Email", SqlDbType.NVarChar, 100) {
                Value = email
            };
            command.Parameters.Add(parameter4);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return (int) parameter.Value;
        }

        public static string UpdateUserName(int UserID, string newuserName)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserUpdateUserName", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = UserID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@NewName", SqlDbType.VarChar, 50) {
                Value = newuserName
            };
            command.Parameters.Add(parameter2);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return "-1";
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return parameter.Value.ToString();
        }

        public static string UpdateUserPassword(int userID, string userPass)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserUpdatePassword", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@Password", SqlDbType.NVarChar, 100) {
                Value = userPass
            };
            command.Parameters.Add(parameter2);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return "-1";
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return parameter.Value.ToString();
        }

        public static bool UpdateUserStyle(int userID, string style)
        {
            SqlConnection sqlconstrSelf = PortalSettings.SqlconstrSelf;
            SqlCommand command = new SqlCommand("p_UserUpdateStyle", sqlconstrSelf) {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter parameter = new SqlParameter("@UserID_1", SqlDbType.Int) {
                Value = userID
            };
            command.Parameters.Add(parameter);
            SqlParameter parameter2 = new SqlParameter("@UserStyle_2", SqlDbType.VarChar, 50) {
                Value = style
            };
            command.Parameters.Add(parameter2);
            try
            {
                if (sqlconstrSelf.State == ConnectionState.Closed)
                {
                    sqlconstrSelf.Open();
                }
                command.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }
            finally
            {
                if (sqlconstrSelf.State == ConnectionState.Open)
                {
                    sqlconstrSelf.Close();
                }
            }
            return true;
        }
    }
}

