﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DeskTopModules
{
    using Intranet.Common;
    using Intranet.Configuration.Settings;
    using Intranet.Security;

    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public class signin : ModuleControls
    {
        protected Button btnLogin;
        protected CheckBox chkSave;
        protected HyperLink hyplnkForget;
        protected HyperLink hypRegister;
        protected Label lblFailLog;
        protected Label lblPass;
        protected Label lblUserID;
        protected Panel pnlSignIn;
        protected RequiredFieldValidator RequiredFieldValidator1;
        public string ReturnPath;
        protected RequiredFieldValidator rfvPassWord;
        protected HtmlTableCell TD1;
        protected TextBox txtPass;
        protected TextBox txtUserName;

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (string.IsNullOrEmpty(this.ReturnPath))
                {
                    //*_*// this.ReturnPath = IntranetUI.BuildUrl("~/default.aspx?tabid=" + base.ModuleConfiguration.GetSettingValue(0x134));
                }
                User user = SignOnController.SignOn(this.txtUserName.Text, this.txtPass.Text, this.chkSave.Checked, this.ReturnPath);
                if (user == null)
                {
                    //*_*// this.lblFailLog.Text = Resource.LoginFailed;
                    if (base.Session["LoginUserName"].ToString() == this.txtUserName.Text)
                    {
                        base.Session["LoginAttempts"] = ((int)base.Session["LoginAttempts"]) + 1;
                    }
                    else
                    {
                        base.Session["LoginUserName"] = this.txtUserName.Text;
                        base.Session["LoginAttempts"] = 1;
                    }
                    if (((int)base.Session["LoginAttempts"]) == PortalSettings.LoginAttempts)
                    {
                        UserDB.LockUser(UserDB.GetSingleUserByName(this.txtUserName.Text).UserID);
                    }
                }
                else if (user.UserID == -2)
                {
                    //*_*//    this.lblFailLog.Text = Resource.UserIsLocked;
                    this.lblFailLog.Text = "کاربر غیر فعال گردیده است";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.IsPostBack)
            {
                base.Session.Add("LoginAttempts", 0);
                base.Session.Add("LoginUserName", "");
            }
            this.SetLanguage();
        }

        private void SetLanguage()
        {
            this.hyplnkForget.NavigateUrl = IntranetUI.BuildUrl("~/DesktopModules/Forgetpass.aspx?mid=" + base.ModuleConfiguration.ModuleID);
            this.hypRegister.NavigateUrl = IntranetUI.BuildUrl("~/Admin/signup.aspx?mID=" + base.ModuleConfiguration.ModuleID);
        }
    }
}
